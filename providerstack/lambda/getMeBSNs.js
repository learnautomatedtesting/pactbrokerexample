
exports.handler = async (event) => {
        // Directly return the desired response
        return {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify([
            { bsn: 123456789 },
            { bsn: 987654321 }
          ]),
        };
      };
