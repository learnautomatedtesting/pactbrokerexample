
exports.handler = async (event) => {
        // Directly return the desired response
        return {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify([
            { bsnnew: 123456789 },
            { bsnnew: 987654321 }
          ]),
        };
      };
