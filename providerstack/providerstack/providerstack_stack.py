from aws_cdk import (
    App,
    Stack,
    aws_lambda as lambda_,
    aws_apigateway as apigateway,
    Duration,
    CfnOutput
    
)
from constructs import Construct




class ProviderstackStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Define the Lambda function
        lambda_function = lambda_.Function(
            self, "GetMeBSNsFunction",
            runtime=lambda_.Runtime.NODEJS_18_X,  # Choose the appropriate runtime
            handler="getMeBSNs.handler",
            code=lambda_.Code.from_asset("./lambda"),
            # Optional: if your function needs more time, you can adjust it here
            timeout=Duration.seconds(30)
        )

        # Define the API Gateway and integrate with the Lambda
        api = apigateway.LambdaRestApi(
            self, "Endpoint",
            handler=lambda_function,
            proxy=False
        )

        items = api.root.add_resource("bsns")
        items.add_method("GET")  # In
        
        
            # Second Lambda function
        lambda_function_2 = lambda_.Function(
            self, "GetMeBSNsFunctionIncorrect",
            runtime=lambda_.Runtime.NODEJS_18_X,
            handler="getMeBSNswrong.handler",  # Adjust the handler to your second function
            code=lambda_.Code.from_asset("./lambdawrong"),  # Adjust the path as necessary
            timeout=Duration.seconds(30)
        )

        # Second API Gateway
        api_2 = apigateway.LambdaRestApi(
            self, "Endpoint2",
            handler=lambda_function_2,
            proxy=False
        )

        items_2 = api_2.root.add_resource("bsns")
        items_2.add_method("GET")

        

                # Output the API Gateway endpoint URL
        CfnOutput(
            self, "APIGatewayEndpointURL",
            value=api.url,
            description="The URL of the API Gateway endpoint",
        )

        
        # Output the second API Gateway endpoint URL
        CfnOutput(
            self, "APIGatewayEndpointURL2",
            value=api_2.url,
            description="The URL of the second API Gateway endpoint",
        )