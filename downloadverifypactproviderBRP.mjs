import axios from 'axios';
import fs from 'fs/promises'; // Use fs.promises for async/await support
import { Verifier } from '@pact-foundation/pact';

const pactFileUrl = 'http://pactst-pactb-oesxbucvqxww-103295103.eu-west-2.elb.amazonaws.com/pacts/provider/BRP-provider/consumer/RWS-consumer/latest';
const localFilePath = './local-pact-file.json'; // Path where you want to save the pact file

// Basic Authentication credentials
const username = 'admin';
const password = 'password';
const auth = Buffer.from(`${username}:${password}`).toString('base64');

async function downloadPactFile() {
  try {
    const response = await axios.get(pactFileUrl, {
      headers: {
        'Authorization': `Basic ${auth}`
      },
      responseType: 'json' // Assuming the response is JSON
    });

    // Save the file locally
    await fs.writeFile(localFilePath, JSON.stringify(response.data));
    console.log(`Pact file downloaded and saved to ${localFilePath}`);
  } catch (error) {
    console.error('Failed to download the pact file:', error.message);
  }
}


async function verifyPacts() {
  // Path to the local pact file
  const localPactPath = './local-pact-file.json';

  const opts = {
    provider: 'BRP-provider',
    providerBaseUrl: 'https://sqsgrhcuji.execute-api.eu-west-2.amazonaws.com/prod',
    pactUrls: [localPactPath], // Use the local file path
    logLevel: 'DEBUG',
  };

  try {
    await new Verifier(opts).verifyProvider();
    console.log('Pact verification complete!');
  } catch (error) {
    console.error('Pact verification failed:', error.message);
    process.exit(1); // Exit
  }
}

async function run() {
  await downloadPactFile();
  await verifyPacts();
}

run();