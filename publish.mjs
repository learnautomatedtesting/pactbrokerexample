import pact from '@pact-foundation/pact-node';
import * as path from 'path';

const pactFilePath = path.resolve(new URL(import.meta.url).pathname, '../pacts/RWS-consumer-BRP-provider.json');

const opts = {
  pactFilesOrDirs: [pactFilePath], // Corrected property name
  pactBroker: 'http://pactst-pactb-oesxbucvqxww-103295103.eu-west-2.elb.amazonaws.com/',
  pactBrokerUsername: 'admin',
  pactBrokerPassword: 'password',
  tags: ['prod', 'test'],
  consumerVersion: '1.0.0',
  consumer: {
    name: 'RWS-Consumer',
    version: '1.0.0',
    description: 'Description of the consumer',
    customField: 'Custom value for the consumer'
  },
  provider: {
    name: 'BSN-Provider',
    version: '1.0.0',
    description: 'Description of the provider',
    customField: 'Custom value for the provider'
  }
};

pact.publishPacts(opts)
  .then(() => {
    console.log('Pact contract publishing complete!');
  })
  .catch(err => {
    console.error('Pact contract publishing failed:', err.message);
  });
