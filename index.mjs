import axios from 'axios';

export const getMeBSNs = async (endpoint) => {
  const { url, port } = endpoint;

  try {
    const response = await axios.get(`http://${url}:${port}/bsns`, {
      headers: {
        Accept: [
          'application/problem+json',
          'application/json',
          'text/plain',
          '*/*',
        ],
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getMeBSN = async (endpoint) => {
  const { url, port } = endpoint;

  try {
    const response = await axios.get(`http://${url}:${port}/bsn/1`, {
      headers: {
        Accept: 'application/json',
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};