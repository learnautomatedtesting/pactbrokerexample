const { Pact } = require('@pact-foundation/pact');
const path = require('path');
const axios = require('axios');
const { Matchers } = require('@pact-foundation/pact');
const { like } = Matchers;

const provider = new Pact({
  consumer: 'MijnApp',
  provider: 'BRPService',
  port: 3001,
  log: path.resolve(process.cwd(), 'logs', 'pact.log'),
  dir: path.resolve(process.cwd(), 'pacts'),
  spec: 2,
});

beforeAll(() => provider.setup());
afterAll(() => provider.finalize());
afterEach(() => provider.verify());

describe('BRP Service', () => {
  test('een verzoek voor persoonsgegevens met BSN 123456789', async () => {
    await provider.addInteraction({
      state: 'data exists for BSN 123456789',
      uponReceiving: 'een verzoek voor persoonsgegevens met BSN 123456789',
      withRequest: {
        method: 'GET',
        path: '/brp/persoonsgegevens',
        query: "bsn=123456789"
      },
      willRespondWith: {
        status: 200,
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          naam: like('Jan Jansen'),
          bsn: like('123456789')
        }
      }
    });

    const requestUrl = `http://localhost:3001/brp/persoonsgegevens?bsn=123456789`;
    console.log(`Making request to: ${requestUrl}`);
    
    try {
      const response = await axios.get(requestUrl,{
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache, no-store, must-revalidate', // Prevent caching
          'Pragma': 'no-cache', // For HTTP/1.0 compatibility
          'Expires': '0' // Older caches
        }});
      console.log('Response received:', response.data);
      expect(response.data).toEqual({
        naam: 'Jan Jansen',
        bsn: '123456789'
      });
    } catch (error) {
      console.error('Error making request:', error.message);
      throw error; // Ensure the test fails if there's an Axios error
    }
  });
});
