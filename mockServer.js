const express = require('express');
const app = express();
const port = 3001; // Use a different port than your actual service

app.use(express.json()); // For parsing application/json

// Mock endpoint
app.get('/brp/persoonsgegevens', (req, res) => {
  // Mock response
  res.json({ naam: 'Jan Jansen', bsn: '123456789' });
});

app.listen(port, () => {
  console.log(`Mock server listening at http://localhost:${port}`);
});