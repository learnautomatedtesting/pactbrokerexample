import { Verifier } from '@pact-foundation/pact';

async function verifyPacts() {
  // Path to the local pact file
  const localPactPath = './local-pact-file.json';

  const opts = {
    provider: 'BRP-provider',
    providerBaseUrl: 'https://sqsgrhcuji.execute-api.eu-west-2.amazonaws.com/prod',
    pactUrls: [localPactPath], // Use the local file path
    logLevel: 'DEBUG',
  };

  try {
    await new Verifier(opts).verifyProvider();
    console.log('Pact verification complete!');
  } catch (error) {
    console.error('Pact verification failed:', error.message);
  }
}

verifyPacts()