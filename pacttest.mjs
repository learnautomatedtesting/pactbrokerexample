

import { expect } from 'chai';
import path from 'path';
import { Pact, Matchers } from '@pact-foundation/pact';
import { getMeBSN, getMeBSNs } from './index.mjs';

const { like } = Matchers;

describe('The BSN API', () => {
  let url = 'localhost';
  const port = 8992;

  const provider = new Pact({
    port: port,
    log: path.resolve(process.cwd(), 'logs', 'mockserver-integration.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    spec: 2,
    consumer: 'RWS-consumer',
    provider: 'BRP-provider',
  //  logLevel: 'trace', // assuming you want to set log level to 'trace'
  });

  const EXPECTED_BODY = [
    {
      bsn: 123456789,
    },
    {
      bsn: 987654321,
    },
  ];

  // Setup the provider
  before(async () => {
    await provider.setup();
  });

  // Write Pact when all tests done
  after(async () => {
    await provider.finalize();
  });

  // verify with Pact, and reset expectations
  afterEach(async () => {
    await provider.verify();
  });

  describe('get /bsns', () => {
    before(async () => {
      const interaction = {
        state: 'i have a list of BSN',
        uponReceiving: 'a request for all BSNs',
        withRequest: {
          method: 'GET',
          path: '/bsns',
          headers: {
            Accept: [
              'application/problem+json',
              'application/json',
              'text/plain',
              '*/*',
            ],
          },
        },
        willRespondWith: {
          status: 200,
          headers: {
            'Content-Type': 'application/json',
          },
          body: [
            { bsn: 123456789 },
            { bsn: 987654321 }
          ],
        },
      };
      await provider.addInteraction(interaction);
    });

    it('returns the correct response', async () => {
      const urlAndPort = {
        url: url,
        port: port,
      };
      const response = await getMeBSNs(urlAndPort);
      console.log(response)
      expect(response).to.eql(EXPECTED_BODY);
    });
  });
});
